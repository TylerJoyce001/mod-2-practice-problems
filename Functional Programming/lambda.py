numbers1 = [1, 2, 3]
numbers2 = [4, 5, 6]

result = map(lambda x, y: x + y, numbers1, numbers2)
print(list(result))



# Defining lambda function
square = lambda x:x * x

# Defining lambda function
# and passing function as an argument
cube = lambda func:func**3


print("square of 2 is :"+str(square(2)))
print("\nThe cube of "+str(square(2))+" is " +str(cube(square(2))))
