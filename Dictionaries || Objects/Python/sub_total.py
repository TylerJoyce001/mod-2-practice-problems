def sub_total(bill):
    return sum(line_item["quantity"] * line_item["item_cost"] for line_item in bill)
