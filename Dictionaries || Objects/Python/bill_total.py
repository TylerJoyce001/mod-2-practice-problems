def bill_total(bill, tax_rate):
    taxables = [line_item for line_item in bill if line_item["taxable"] is True]
    nontaxables = [line_item for line_item in bill if line_item["taxable"] is False]
    nontaxable_subtotal = sum(line_item["quantity"] * line_item["item_cost"] for line_item in nontaxables)
    taxable_subtotal = sum(line_item["quantity"] * line_item["item_cost"] for line_item in taxables)
    return taxable_subtotal + (tax_rate * taxable_subtotal) + nontaxable_subtotal
