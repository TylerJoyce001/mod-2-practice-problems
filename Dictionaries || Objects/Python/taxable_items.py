def taxable_items(bill):
    return [line_item for line_item in bill if line_item["taxable"] is True]
