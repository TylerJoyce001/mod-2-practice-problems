def merge_dictionaries(d1, d2):
    d1.update(d2)
    return d1

d1 = {"a":1, "b":2}
d2 = {"b":"bbb", "c": "ccc"}
d3 = merge_dictionaries(d1, d2)
print(d3) # --> {"a":1, "b":"bbb", "c": "ccc"})
