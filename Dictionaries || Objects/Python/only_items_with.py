def only_items_with(items, filters):
    result = []

    for item in items:
        for key in filters.keys():
            if item[key] == filters[key]:
                result.append(item)
                break
    return result

items = [
    {"color":"blue", "size":"small"},
    {"color":"red", "size":"small"},
    {"color":"purple", "size":"medium"},
    {"color":"green", "size":"large"},
]
filters = {
    "color": "blue",
    "size": "medium"
}

result = only_items_with(items, filters)
print(result) # --> [

