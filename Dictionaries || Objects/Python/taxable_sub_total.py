def taxable_sub_total(bill):
    taxables = [line_item for line_item in bill if line_item["taxable"] is True]
    return sum(line_item["quantity"] * line_item["item_cost"] for line_item in taxables)
