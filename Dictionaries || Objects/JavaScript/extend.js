function extend(obj1, obj2) {
    for (key in obj2) {
        if (obj1[key] === undefined){
            obj1[key] = obj2[key];
        }
    }
  }
