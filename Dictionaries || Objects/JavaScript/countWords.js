function countWords(stringOfWords) {
    if (stringOfWords === "") {
        return {};
    }
    let counts = {}
    let words = stringOfWords.split(" ")
    for (let i=0; i < words.length; i++) {
        let word = words[i];
        if (counts[word] === undefined){
            counts[word] = 1;
        } else {
            counts[word]++;
       }
    } return counts;
  }
