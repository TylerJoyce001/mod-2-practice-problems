function objectFromArray(array) {
    let obj = {};
    for (let i = 0; i < array.length; i+=2){
        obj[array[i]] = array[i+1];
        if(obj[array[i]] === undefined){
            delete obj[array[i]];
        }
    } return obj;
} 
