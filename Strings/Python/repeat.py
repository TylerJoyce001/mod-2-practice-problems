def repeats(s):
    return s[:(len(s)//2)] == s[(len(s)//2):]
