function diyMin(numbers) {
    if (numbers.length === 0){
        return NaN;
    }
    let min = null;
    for (let n of numbers){
        if (min === null) {
            min = n;
        } else if (min > n) {
            min = n;
        }
    } return min;
}
