function diyMax(numbers) {
    if (numbers.length === 0){
        return NaN;
    }
    let max = numbers[0];
    for (let n of numbers){
        if (max < n){
            max = n;
        }
    } return max;
}
