def sum_evens(numbers):
    return sum(n for n in numbers if n % 2 == 0)
