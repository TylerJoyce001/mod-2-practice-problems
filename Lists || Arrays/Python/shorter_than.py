def shorter_than_X(strings, max_length):
    return [string for string in strings if len(string) < max_length]
