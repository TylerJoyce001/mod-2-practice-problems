import json
def find_big_space_objects(space_objects):
    big_objects = []
    for obj in space_objects:
        if obj['estimated_diameter'] < 1:
            continue
        new_dict = {}
        new_dict['estimated_diameter'] = obj['estimated_diameter']
        new_dict['impact_probability'] = obj['impact_probability']
        big_objects.append(json.dumps(new_dict))
    return big_objects
