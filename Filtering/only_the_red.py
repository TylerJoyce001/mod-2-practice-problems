def only_the_red(cars):
    return [car for car in cars if car["color"] is "red"]
