# def mean(numbers):
#     sum = 0
#     for n in numbers:
#         sum += n
#     return sum/len(numbers)

# from functools import reduce

# def mean2(numbers):
#     return reduce(lambda a, b: a+b, numbers)/len(numbers)

def mean(numbers):
    return sum(numbers)/len(numbers)
