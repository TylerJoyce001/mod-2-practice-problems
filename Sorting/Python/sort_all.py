def sort(items):
    while True:
        swap= False
        for i in range(len(items) - 1):
            if items[i] > items[i+1]:
                (items[i], items[i+1]) = (items[i+1], items[i])
                swap = True
        if not swap:
            break
    return items
